

/*
   ADC read on A0

   On A1 : a reistors divider 100K + 10K

   On A2 : a reistors divider 1M + 10K

   This software reads the ADC using polling with HAL_ADC_Start.

   Just configure adc.h :

   #define CHANNELS   SINGLE_CHANNEL                  // analog input A0 (PA0)
   or
   #define CHANNELS   DUAL_CHANNEL                    // analog input A0 (PA0) + A1 (PA1)

   #define SAMPLETIME      ADC_SAMPLETIME_480CYCLES   // 480 clock cycles
   or
   #define SAMPLETIME      ADC_SAMPLETIME_122CYCLES   // 122 clock cycles
   or
   #define SAMPLETIME      ADC_SAMPLETIME_28CYCLES    // 28 clock cycles

   Board : F401RE

   The results :

   the ADC channel is reconfigured before each reading

   with ADC_SAMPLETIME_480CYCLES
     SINGLE_CHANNEL:  85333 samples/s
     DUAL_CHANNEL:    20352 samples/s
   with ADC_SAMPLETIME_122CYCLES
     SINGLE_CHANNEL:  341333 samples/s
     DUAL_CHANNEL:    24899 samples/s
   with ADC_SAMPLETIME_28CYCLES
     SINGLE_CHANNEL:  744727 samples/s
     DUAL_CHANNEL:    25965 samples/s

*/

#include "adc.h"

#define VREF      3.3
// pont diviseur 97.1KΩ / 9.93KΩ
#define DIVIDER   0.0928
#define SHUNT     10000

void setup(void)
{
  Serial.begin(115200);
  Serial.println("ADC DEMO");
  pinMode(LED_BUILTIN, OUTPUT);
  adc_configureGpios();
  if (adc_configure() != true) {
    Serial.println("adc_configure failed");
  }
  Serial.println("adc_configure OK");

  uint32_t start = millis();
#if CHANNELS == SINGLE_CHANNEL
  testPollingSingleChannel();
#else
  testPollingTwoChannels();
#endif
  uint32_t elapsed = millis() - start;

  adcValueA0 /= TEST_LENGTH;
#if CHANNELS == DUAL_CHANNEL
  adcValueA1 /= TEST_LENGTH;
#endif
  Serial.print("ADC0: "); Serial.println(adcValueA0);
  float voltage = (float)adcValueA0 * VREF / 4096 / DIVIDER;
  Serial.print("Voltage: ");
  Serial.print(voltage);
  Serial.println(" V");
#if CHANNELS == DUAL_CHANNEL
  Serial.print("ADC1: "); Serial.println(adcValueA1);
  float current = (float)adcValueA1 * VREF / 4096 / SHUNT * 1000000;
  Serial.print("Current: ");
  Serial.print(current);
  Serial.println(" µA");
#endif
  Serial.print("elapsed time ");
  Serial.print(elapsed);
  Serial.println(" ms");
  Serial.print("samples ");
  Serial.println(TEST_LENGTH);
  Serial.print("ADC speed: ");
  Serial.print(adcCounter * 1000L / elapsed);
  Serial.println(" SPS");
}

void loop(void)
{
  digitalWrite(LED_BUILTIN, HIGH);
  delay(250);
  digitalWrite(LED_BUILTIN, LOW);
  delay(250);
}

ADC_ChannelConfTypeDef adcChannel[2] =
{
  {ADC_CHANNEL_0, 1, SAMPLETIME, 0},
  {ADC_CHANNEL_1, 1, SAMPLETIME, 0}
};

void testPollingSingleChannel(void)
{
  uint16_t value = 0;

  if (HAL_ADC_ConfigChannel(&adcHandle, &adcChannel[0]) != HAL_OK) {
    Serial.println("HAL_ADC_ConfigChannel failed");
    return;
  }
  if (HAL_ADC_Start(&adcHandle) != HAL_OK) {
    Serial.println("HAL_ADC_Start failed");
    return;
  }
  for (int x = 0 ; x < TEST_LENGTH ; x++) {
    if (HAL_ADC_PollForConversion(&adcHandle, 1000) != HAL_OK) {
      Serial.println("HAL_ADC_PollForConversion failed");
      return ;
    }
    if ((HAL_ADC_GetState(&adcHandle) & HAL_ADC_STATE_REG_EOC) == HAL_ADC_STATE_REG_EOC) {
      adcValueA0 += HAL_ADC_GetValue(&adcHandle);
    }
    adcCounter++;
  }
  if (HAL_ADC_Stop(&adcHandle) != HAL_OK) {
    Serial.println("HAL_ADC_Stop failed");
    return;
  }
}

void testPollingTwoChannels(void)
{
  for (int x = 0 ; x < TEST_LENGTH ; x++) {
    adcValueA0 += adc_read(0);
    adcValueA1 += adc_read(1);
    adcCounter += 2;
  }
}

uint16_t adc_read(uint8_t channel)
{
  __IO uint16_t value = 0;

  if (HAL_ADC_ConfigChannel(&adcHandle, &adcChannel[channel]) != HAL_OK) {
    Serial.println("HAL_ADC_ConfigChannel failed");
    return 0;
  }
  if (HAL_ADC_Start(&adcHandle) != HAL_OK) {
    return 0;
  }
  if (HAL_ADC_PollForConversion(&adcHandle, 10) != HAL_OK) {
    return 0;
  }
  if ((HAL_ADC_GetState(&adcHandle) & HAL_ADC_STATE_REG_EOC) == HAL_ADC_STATE_REG_EOC) {
    value = HAL_ADC_GetValue(&adcHandle);
  }
  if (HAL_ADC_Stop(&adcHandle) != HAL_OK) {
    return 0;
  }
  return value;
}

