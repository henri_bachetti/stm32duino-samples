
#include <Arduino.h>
#include <numeric>

#include "adc.h"

ADC_HandleTypeDef adcHandle;
uint32_t adcValueA0, adcValueA1;
volatile int adcCounter;

int adc_configureGpios(void)
{
  GPIO_InitTypeDef gpioInit;

  __GPIOA_CLK_ENABLE();
  gpioInit.Pin = GPIO_PIN_0;
  gpioInit.Mode = GPIO_MODE_ANALOG;
  gpioInit.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &gpioInit);
#if CHANNELS == DUAL_CHANNEL
  gpioInit.Pin = GPIO_PIN_1;
  gpioInit.Mode = GPIO_MODE_ANALOG;
  gpioInit.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &gpioInit);
#endif
  return true;
}

int adc_configure(void)
{
  __ADC1_CLK_ENABLE();
  adcHandle.Instance = ADC1;
  adcHandle.Init.ClockPrescaler        = ADC_CLOCKPRESCALER_PCLK_DIV2;  /* Asynchronous clock mode, input ADC clock divided */
  adcHandle.Init.Resolution            = ADC_RESOLUTION_12B;            /* 12-bit resolution for converted data */
  adcHandle.Init.ScanConvMode          = DISABLE;                       /* Sequencer disabled (ADC conversion on only 1 channel:*/
  adcHandle.Init.EOCSelection          = DISABLE;                       /* EOC flag picked-up to indicate conversion end */
  adcHandle.Init.ExternalTrigConvEdge  = ADC_EXTERNALTRIGCONVEDGE_NONE; /* Parameter discarded because software trigger chosen */
  adcHandle.Init.ExternalTrigConv      = ADC_EXTERNALTRIGCONV_T1_CC1;
  adcHandle.Init.DMAContinuousRequests = DISABLE;                       /* DMA one-shot mode selected (not applied to this example) */
  adcHandle.Init.DataAlign             = ADC_DATAALIGN_RIGHT;           /* Right-alignment for converted data channel set on rank 1) */
  adcHandle.Init.ContinuousConvMode    = ENABLE;                        /* Continuous mode disabled to have only 1 conversion at each conversion trig */
  adcHandle.Init.DiscontinuousConvMode = DISABLE;                       /* Parameter discarded because sequencer is disabled */
  adcHandle.State = HAL_ADC_STATE_RESET;
  adcHandle.Init.NbrOfConversion       = 1;                             /* Specifies the number of ranks that will be converted within the regular group sequencer. */
  adcHandle.Init.NbrOfDiscConversion   = 0;                             /* Parameter discarded because sequencer is disabled */
  if (HAL_ADC_Init(&adcHandle) != HAL_OK) {
    return false;
  }
  return true;
}

