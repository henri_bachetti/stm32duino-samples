
#define SINGLE_CHANNEL  1
#define DUAL_CHANNEL    2

#define CHANNELS        DUAL_CHANNEL
#define SAMPLETIME      ADC_SAMPLETIME_112CYCLES

#define TEST_LENGTH     8192

extern uint32_t adcValueA0, adcValueA1;
extern volatile int adcCounter;
extern ADC_HandleTypeDef adcHandle;

int adc_configureGpios(void);
int adc_configure();

