
/*
   ADC read on A0 and A1 with DMA
   
   A demo to demonstrate DMA with two channels and 1 ADC

   On A0 : a reistors divider 100K + 10K

   On A1 : a reistors divider 1M + 10K

   This software reads the ADC using polling with HAL_ADC_Start_DMA.

   Board : F429ZI with ADC1

   Just configure main.h :

   #define SAMPLETIME      ADC_SAMPLETIME_56CYCLES    // 56 clock cycles
   or
   #define SAMPLETIME      ADC_SAMPLETIME_28CYCLES    // 28 clock cycles
   or
   #define SAMPLETIME      ADC_SAMPLETIME_15CYCLES    // 15 clock cycles

   The results :

   ADC_SAMPLETIME_56CYCLES:    156328 samples/s
   ADC_SAMPLETIME_28CYCLES:    263473 samples/s
   ADC_SAMPLETIME_15CYCLES:    401360 samples/s

   This is built with PlatformIO.

*/

#include "main.h"

float readVoltage(void);
float readCurrent(void);
void readSPS(void);

void setup()
{
  Serial.begin(115200);
  pinMode (LED_GREEN, OUTPUT);
  pinMode (LED_BLUE, OUTPUT);
  pinMode (LED_RED, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  adc_init_gpios();
  if (dma_init() != true) {
    Serial.println("dma_init failed");
  }
  if (adc_init() != true) {
    Serial.println("adc_init failed");
  }
  readSPS();
  digitalWrite(LED_BUILTIN, LOW);
}

void loop() {
  digitalWrite(LED_BUILTIN, HIGH);
  float voltage = readVoltage();
  Serial.print("Voltage: ");
  Serial.print(voltage);
  Serial.println(" V");
  float current = readCurrent();
  Serial.print("Current: ");
  Serial.print(current);
  Serial.println(" µA");
  digitalWrite(LED_BUILTIN, LOW);
  delay(2000);
}

float readVoltage(void)
{
  uint32_t value = 0;
  
  for (int i = 0 ; i < NSAMPLES ; i++) {
    value += adcValue[0];
  }
  value /= NSAMPLES;
  Serial.print("adcValueA0: ");
  Serial.println(value);
  float voltage = (float)value * VREF / 4096 / DIVIDER;
  return voltage;
}

float readCurrent(void)
{
  uint32_t value = 0;
  
  for (int i = 0 ; i < NSAMPLES ; i++) {
    value += adcValue[1];
  }
  value /= NSAMPLES;
  Serial.print("adcValueA1: ");
  Serial.println(value);
  float current = (float)value * VREF / 4096 / SHUNT * 1000000;
  return current;
}

void readSPS(void)
{
  uint32_t value = 0;
  
  uint32_t start = millis();
  while (adcCounter < TEST) {
    value += adcValue[0];
  }
  uint32_t elapsed = millis() - start;
  Serial.print("elapsed time ");
  Serial.print(elapsed);
  Serial.println(" µs");
  Serial.print("ADC speed: ");
  Serial.print(adcCounter * 1000L / elapsed);
  Serial.println(" SPS");
}
