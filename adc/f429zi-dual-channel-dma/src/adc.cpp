
#include <numeric>

#include "main.h"

ADC_HandleTypeDef adcHandle;
DMA_HandleTypeDef dmaHandle;

__IO uint32_t adcValue[ADC_BUFFER_LENGTH];
__IO int adcCounter;
uint32_t adcBuffer[ADC_BUFFER_LENGTH];

ADC_ChannelConfTypeDef adcChannel[2] =
{
  {ADC_CHANNEL_3, 1, SAMPLE_TIME, 0},
  {ADC_CHANNEL_10, 2, SAMPLE_TIME, 0}
};

void adc_init_gpios(void)
{
  GPIO_InitTypeDef GPIO_InitStruct;

  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  GPIO_InitStruct.Pin = GPIO_PIN_3;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
  GPIO_InitStruct.Pin = GPIO_PIN_0;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
}

int dma_init(void)
{
  __DMA2_CLK_ENABLE(); 
  dmaHandle.Instance = DMA2_Stream4;
  dmaHandle.Init.Channel  = DMA_CHANNEL_0;
  dmaHandle.Init.Direction = DMA_PERIPH_TO_MEMORY;
  dmaHandle.Init.PeriphInc = DMA_PINC_DISABLE;
  dmaHandle.Init.MemInc = DMA_MINC_ENABLE;
  dmaHandle.Init.PeriphDataAlignment = DMA_PDATAALIGN_WORD;
  dmaHandle.Init.MemDataAlignment = DMA_MDATAALIGN_WORD;
  dmaHandle.Init.Mode = DMA_CIRCULAR;
  dmaHandle.Init.Priority = DMA_PRIORITY_HIGH;
  dmaHandle.Init.FIFOMode = DMA_FIFOMODE_DISABLE;         
  dmaHandle.Init.FIFOThreshold = DMA_FIFO_THRESHOLD_HALFFULL;
  dmaHandle.Init.MemBurst = DMA_MBURST_SINGLE;
  dmaHandle.Init.PeriphBurst = DMA_PBURST_SINGLE; 
  if (HAL_DMA_Init(&dmaHandle) != HAL_OK) {
    return false;
  }
  __HAL_LINKDMA(&adcHandle, DMA_Handle, dmaHandle);
  HAL_NVIC_SetPriority(DMA2_Stream4_IRQn, 0, 0);   
  HAL_NVIC_EnableIRQ(DMA2_Stream4_IRQn);
  return true;
}

  int adc_init(void)
{
  __HAL_RCC_ADC1_CLK_ENABLE();
  __HAL_RCC_ADC2_CLK_ENABLE();
  HAL_NVIC_SetPriority(ADC_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(ADC_IRQn);
  adcHandle.Instance                   = ADC1;
  adcHandle.Init.ClockPrescaler        = ADC_CLOCK_DIV;
  adcHandle.Init.Resolution            = ADC_RESOLUTION_12B;
  adcHandle.Init.ScanConvMode          = ENABLE;                       /* Sequencer disabled (ADC conversion on only 1 channel: channel set on rank 1) */
  adcHandle.Init.ContinuousConvMode    = ENABLE;                        /* Continuous mode disabled to have only 1 conversion at each conversion trig */
  adcHandle.Init.DiscontinuousConvMode = DISABLE;                       /* Parameter discarded because sequencer is disabled */
  adcHandle.Init.NbrOfDiscConversion   = 0;
  adcHandle.Init.ExternalTrigConvEdge  = ADC_EXTERNALTRIGCONVEDGE_NONE;        /* Conversion start trigged at each external event */
  adcHandle.Init.ExternalTrigConv      = ADC_EXTERNALTRIGCONV_T1_CC1;
  adcHandle.Init.DataAlign             = ADC_DATAALIGN_RIGHT;
  adcHandle.Init.NbrOfConversion       = 2;
  adcHandle.Init.DMAContinuousRequests = ENABLE;
  adcHandle.Init.EOCSelection          = DISABLE;
  if (HAL_ADC_Init(&adcHandle) != HAL_OK) {
    return false;
  }
  if (HAL_ADC_ConfigChannel(&adcHandle, &adcChannel[0]) != HAL_OK) {
    return false;
  }
  if (HAL_ADC_ConfigChannel(&adcHandle, &adcChannel[1]) != HAL_OK) {
    return 0;
  }
  if (HAL_ADC_Start_DMA(&adcHandle, (uint32_t *)&adcValue, sizeof(adcValue)) != HAL_OK) {
    return 0;
  }
  return true;
}

extern "C" {

  void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* adcHandle)
  {
      adcCounter += 1; 
  }

  void DMA2_Stream4_IRQHandler()
  {
    HAL_DMA_IRQHandler(&dmaHandle);
  }

  void ADC_IRQHandler(void)
  {
    HAL_ADC_IRQHandler(&adcHandle);
  }
}
