
#include <Arduino.h>

#ifndef __MAIN_H
#define __MAIN_H

#define ADC_CLOCK_DIV     ADC_CLOCKPRESCALER_PCLK_DIV4
#define SAMPLE_TIME       ADC_SAMPLETIME_56CYCLES
#define ADC_BUFFER_LENGTH 2

extern __IO int adcCounter;
extern __IO uint32_t adcValue[ADC_BUFFER_LENGTH];
extern ADC_HandleTypeDef    adcHandle;

#define VREF      3.3
// pont diviseur 97.1KΩ / 9.93KΩ
#define DIVIDER   0.0928
#define SHUNT     10000
#define NSAMPLES  3
#define TEST      10000

void adc_init_gpios(void);
int dma_init(void);
int adc_init(void);

#endif /* __MAIN_H */
