
#include "main.h"

ADC_HandleTypeDef    hadcVoltage;
ADC_HandleTypeDef    hadcCurrent;
__IO uint16_t uhADCxConvertedValue = 0;

ADC_ChannelConfTypeDef adcChannel[2] =
{
  {ADC_CHANNEL_5, 1, SAMPLE_TIME, ADC_SINGLE_ENDED, ADC_OFFSET_NONE, 0},
  {ADC_CHANNEL_6, 1, SAMPLE_TIME, ADC_SINGLE_ENDED, ADC_OFFSET_NONE, 0}
};

void adc_init_gpios(void)
{
  GPIO_InitTypeDef GPIO_InitStruct;
  __HAL_RCC_GPIOA_CLK_ENABLE();
  GPIO_InitStruct.Pin = GPIO_PIN_0;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG_ADC_CONTROL;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
  GPIO_InitStruct.Pin = GPIO_PIN_1;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
}

int adc_init(void)
{
  __HAL_RCC_ADC_CLK_ENABLE();
  hadcVoltage.Instance                   = ADC1;
  hadcVoltage.Init.ClockPrescaler        = ADC_CLOCK_DIV;
  hadcVoltage.Init.Resolution            = ADC_RESOLUTION_12B;
  hadcVoltage.Init.ScanConvMode          = DISABLE;                       /* Sequencer disabled (ADC conversion on only 1 channel: channel set on rank 1) */
  hadcVoltage.Init.ContinuousConvMode    = ENABLE;                        /* Continuous mode disabled to have only 1 conversion at each conversion trig */
  hadcVoltage.Init.DiscontinuousConvMode = DISABLE;                       /* Parameter discarded because sequencer is disabled */
  hadcVoltage.Init.NbrOfDiscConversion   = 0;
  hadcVoltage.Init.ExternalTrigConvEdge  = ADC_EXTERNALTRIGCONVEDGE_NONE;        /* Conversion start trigged at each external event */
  hadcVoltage.Init.ExternalTrigConv      = ADC_SOFTWARE_START;
  hadcVoltage.Init.EOCSelection          = ADC_EOC_SINGLE_CONV;
  hadcVoltage.Init.LowPowerAutoWait      = ENABLE;
  hadcVoltage.Init.DataAlign             = ADC_DATAALIGN_RIGHT;
  hadcVoltage.Init.NbrOfConversion       = 1;
  hadcVoltage.Init.DMAContinuousRequests = DISABLE;
  if (HAL_ADC_Init(&hadcVoltage) != HAL_OK) {
    return false;
  }
  if (HAL_ADCEx_Calibration_Start(&hadcVoltage, ADC_SINGLE_ENDED) != HAL_OK) {
    return false;
  }
  if (HAL_ADC_ConfigChannel(&hadcVoltage, &adcChannel[0]) != HAL_OK) {
    return false;
  }
  if (HAL_ADC_Start(&hadcVoltage) != HAL_OK) {
    return false;
  }
  hadcCurrent.Instance                   = ADC2;
  hadcCurrent.Init.ClockPrescaler        = ADC_CLOCK_DIV;
  hadcCurrent.Init.Resolution            = ADC_RESOLUTION_12B;
  hadcCurrent.Init.ScanConvMode          = DISABLE;                       /* Sequencer disabled (ADC conversion on only 1 channel: channel set on rank 1) */
  hadcCurrent.Init.ContinuousConvMode    = ENABLE;                        /* Continuous mode disabled to have only 1 conversion at each conversion trig */
  hadcCurrent.Init.DiscontinuousConvMode = DISABLE;                       /* Parameter discarded because sequencer is disabled */
  hadcCurrent.Init.NbrOfDiscConversion   = 0;
  hadcCurrent.Init.ExternalTrigConvEdge  = ADC_EXTERNALTRIGCONVEDGE_NONE;        /* Conversion start trigged at each external event */
  hadcCurrent.Init.ExternalTrigConv      = ADC_SOFTWARE_START;
  hadcCurrent.Init.EOCSelection          = ADC_EOC_SINGLE_CONV;
  hadcVoltage.Init.LowPowerAutoWait      = ENABLE;
  hadcCurrent.Init.DataAlign             = ADC_DATAALIGN_RIGHT;
  hadcCurrent.Init.NbrOfConversion       = 1;
  hadcCurrent.Init.DMAContinuousRequests = DISABLE;
  if (HAL_ADC_Init(&hadcCurrent) != HAL_OK) {
    return false;
  }
  if (HAL_ADCEx_Calibration_Start(&hadcCurrent, ADC_SINGLE_ENDED) != HAL_OK) {
    return false;
  }
  if (HAL_ADC_ConfigChannel(&hadcCurrent, &adcChannel[1]) != HAL_OK) {
    return 0;
  }
  if (HAL_ADC_Start(&hadcCurrent) != HAL_OK) {
    return false;
  }
  return true;
}

