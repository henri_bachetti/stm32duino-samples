
/*
   ADC read on A1 and A2 with  polling
   
   A demo to demonstrate fast polling with two ADCs

   On A1 : a reistors divider 100K + 10K
   On A1 : a reistors divider 1M + 10K

   This software reads the ADC using polling with HAL_ADC_Start.

   Board : L476 with ADC1 and ADC2

   Just configure main.h :

   #define SAMPLETIME      ADC_SAMPLETIME_47CYCLES_5    // 47.5 clock cycles
   or
   #define SAMPLETIME      ADC_SAMPLETIME_12CYCLES_5    // 12.5 clock cycles

   The results :

   ADC_SAMPLETIME_47CYCLES_5:    133333 samples/s
   ADC_SAMPLETIME_12CYCLES_5:    256410 samples/s

   This is built with PlatformIO.

*/

#include "main.h"

#define VREF      3.3
// pont diviseur 97.1KΩ / 9.93KΩ
#define DIVIDER   0.0928
#define SHUNT     10000
#define NSAMPLES  3
#define TEST      10000

float readVoltage(void);
float readCurrent(void);
static uint16_t adc_read(ADC_HandleTypeDef *hadc);
void readSPS(void);

void setup()
{
  Serial.begin(115200);
  adc_init_gpios();
  if (adc_init() != true) {
    Serial.println("adc_init failed");
  }
  Serial.println("adc_init OK");
  pinMode (LED_BUILTIN, OUTPUT);
  readSPS();
}

void loop() {
  float voltage = readVoltage();
  Serial.print("Voltage: ");
  Serial.print(voltage);
  Serial.println(" V");
  float current = readCurrent();
  Serial.print("Current: ");
  Serial.print(current);
  Serial.println(" µA");
  delay(4000);
}

float readVoltage(void)
{
  uint32_t value = 0;
  
  for (int i = 0 ; i < NSAMPLES ; i++) {
    value += adc_read(&hadcVoltage);
  }
  value /= NSAMPLES;
  Serial.print("adc_read(A0): ");
  Serial.println(value);
  float voltage = (float)value * VREF / 4096 / DIVIDER;
  return voltage;
}

float readCurrent(void)
{
  uint32_t value = 0;
  
  for (int i = 0 ; i < NSAMPLES ; i++) {
    value += adc_read(&hadcCurrent);
  }
  value /= NSAMPLES;
  Serial.print("adc_read(A1): ");
  Serial.println(value);
  float current = (float)value * VREF / 4096 / SHUNT * 1000000;
  return current;
}

uint16_t adc_read(ADC_HandleTypeDef *hadc)
{
  __IO uint16_t value = 0;

  if (HAL_ADC_PollForConversion(hadc, 100000) != HAL_OK) {
    Serial.println("HAL_ADC_PollForConversion failed");
    return 0;
  }
  if ((HAL_ADC_GetState(hadc) & HAL_ADC_STATE_REG_EOC) == HAL_ADC_STATE_REG_EOC) {
    value = HAL_ADC_GetValue(hadc);
  }
  return value;
}

void readSPS(void)
{
  uint32_t value = 0;
  
  uint32_t start = millis();
  for (int i = 0 ; i < TEST ; i++) {
    value += adc_read(&hadcVoltage);
  }
  uint32_t elapsed = millis() - start;
  Serial.print("elapsed time ");
  Serial.print(elapsed);
  Serial.println(" µs");
  Serial.print("ADC speed: ");
  Serial.print(TEST * 1000L / elapsed);
  Serial.println(" SPS");
}
