
#include <Arduino.h>

#ifndef __MAIN_H
#define __MAIN_H

#define ADC_CLOCK_DIV   ADC_CLOCKPRESCALER_PCLK_DIV4
#define SAMPLE_TIME     ADC_SAMPLETIME_15CYCLES

extern __IO uint16_t uhADCxConvertedValue;
extern ADC_HandleTypeDef    hadcVoltage;
extern ADC_HandleTypeDef    hadcCurrent;

void adc_init_gpios(void);
int adc_init(void);

#endif /* __MAIN_H */
