
/*
   ADC read on A0 and A1 with  polling
   
   A demo to demonstrate fast polling with two ADCs

   On A0 : a reistors divider 100K + 10K

   On A1 : a reistors divider 1M + 10K

   This software reads the ADC using polling with HAL_ADC_Start.

   Board : F429ZI with ADC1 and ADC2

   Just configure main.h :

   #define SAMPLETIME      ADC_SAMPLETIME_56CYCLES    // 56 clock cycles
   or
   #define SAMPLETIME      ADC_SAMPLETIME_15CYCLES     // 15 clock cycles

   The results per channel:

   ADC_SAMPLETIME_56CYCLES:    312500 samples/s
   ADC_SAMPLETIME_15CYCLES:    833333 samples/s

   This is built with PlatformIO.

*/

#include "main.h"

#define VREF      3.3
// pont diviseur 97.1KΩ / 9.93KΩ
#define DIVIDER   0.0928
#define SHUNT     10000
#define NSAMPLES  3
#define TEST      10000

float readVoltage(void);
float readCurrent(void);
static uint16_t adc_read(ADC_HandleTypeDef *hadc);
void readSPS(void);

void setup()
{
  Serial.begin(115200);
  pinMode (LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  adc_init_gpios();
  if (adc_init() != true) {
    Serial.println("adc_init failed");
  }
  readSPS();
  digitalWrite(LED_BUILTIN, LOW);
}

void loop() {
  digitalWrite(LED_BUILTIN, HIGH);
  float voltage = readVoltage();
  Serial.print("Voltage: ");
  Serial.print(voltage);
  Serial.println(" V");
  float current = readCurrent();
  Serial.print("Current: ");
  Serial.print(current);
  Serial.println(" µA");
  digitalWrite(LED_BUILTIN, LOW);
  delay(2000);
}

float readVoltage(void)
{
  uint32_t value = 0;
  
  for (int i = 0 ; i < NSAMPLES ; i++) {
    value += adc_read(&hadcVoltage);
  }
  value /= NSAMPLES;
  Serial.print("adc_read(A0): ");
  Serial.println(value);
  float voltage = (float)value * VREF / 4096 / DIVIDER;
  return voltage;
}

float readCurrent(void)
{
  uint32_t value = 0;
  
  for (int i = 0 ; i < NSAMPLES ; i++) {
    value += adc_read(&hadcCurrent);
  }
  value /= NSAMPLES;
  Serial.print("adc_read(A1): ");
  Serial.println(value);
  float current = (float)value * VREF / 4096 / SHUNT * 1000000;
  return current;
}

uint16_t adc_read(ADC_HandleTypeDef *hadc)
{
  uint16_t value = 0;

  if (HAL_ADC_PollForConversion(hadc, 10) != HAL_OK) {
    return 0;
  }
  if ((HAL_ADC_GetState(hadc) & HAL_ADC_STATE_REG_EOC) == HAL_ADC_STATE_REG_EOC) {
    value = HAL_ADC_GetValue(hadc);
  }
  return value;
}

void readSPS(void)
{
  uint32_t value = 0;
  
  uint32_t start = millis();
  for (int i = 0 ; i < TEST ; i++) {
    value += adc_read(&hadcVoltage);
  }
  uint32_t elapsed = millis() - start;
  Serial.print("elapsed time ");
  Serial.print(elapsed);
  Serial.println(" µs");
  Serial.print("ADC speed: ");
  Serial.print(TEST * 1000L / elapsed);
  Serial.println(" SPS");
}
