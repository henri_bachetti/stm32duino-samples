
#include <Arduino.h>

#ifndef __MAIN_H
#define __MAIN_H

#define ADC_CLOCK_DIV   ADC_CLOCKPRESCALER_PCLK_DIV4
#define SAMPLE_TIME     ADC_SAMPLETIME_28CYCLES

extern __IO uint16_t adcValueA0, adcValueA1;
extern __IO int adcCounterA0, adcCounterA1;
extern ADC_HandleTypeDef    hadcVoltage;
extern ADC_HandleTypeDef    hadcCurrent;

#define VREF      3.3
// pont diviseur 97.1KΩ / 9.93KΩ
#define DIVIDER   0.0928
#define SHUNT     10000
#define NSAMPLES  3
#define TEST      10000

void adc_init_gpios(void);
int adc_init(void);

#endif /* __MAIN_H */
