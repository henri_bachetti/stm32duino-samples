
/*
   ADC read on A0 and A1 with interrupts
   
   A demo to demonstrate interrupts with two ADCs

   On A0 : a resistors divider 100K + 10K

   On A1 : a resistors divider 1M + 10K

   This software reads the ADC using interrupts with HAL_ADC_Start_IT.

   Board : F429ZI with ADC1 and ADC2

   Just configure main.h :

   #define SAMPLETIME      ADC_SAMPLETIME_56CYCLES    // 56 clock cycles
   or
   #define SAMPLETIME      ADC_SAMPLETIME_28CYCLES    // 28 clock cycles
   or
   #define SAMPLETIME      ADC_SAMPLETIME_15CYCLES    // 15 clock cycles

   The results per channel :

   ADC_SAMPLETIME_56CYCLES:    313406 samples/s
   ADC_SAMPLETIME_28CYCLES:    543315 samples/s
   ADC_SAMPLETIME_15CYCLES:    main thread never executes (the LED does not blink)
                               too much time is spent in the interrupt

   This is built with PlatformIO.

*/

#include "main.h"

float readVoltage(void);
float readCurrent(void);
void readSPS(void);

void setup()
{
  Serial.begin(115200);
  pinMode (LED_GREEN, OUTPUT);
  pinMode (LED_BLUE, OUTPUT);
  pinMode (LED_RED, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  adc_init_gpios();
  if (adc_init() != true) {
    Serial.println("adc_init failed");
  }
  readSPS();
  digitalWrite(LED_BUILTIN, LOW);
}

void loop() {
  digitalWrite(LED_BUILTIN, HIGH);
  float voltage = readVoltage();
  Serial.print("Voltage: ");
  Serial.print(voltage);
  Serial.println(" V");
  float current = readCurrent();
  Serial.print("Current: ");
  Serial.print(current);
  Serial.println(" µA");
  digitalWrite(LED_BUILTIN, LOW);
  delay(2000);
}

float readVoltage(void)
{
  uint32_t value = 0;
  
  for (int i = 0 ; i < NSAMPLES ; i++) {
    value += adcValueA0;
  }
  value /= NSAMPLES;
  Serial.print("adcValueA0: ");
  Serial.println(value);
  float voltage = (float)value * VREF / 4096 / DIVIDER;
  return voltage;
}

float readCurrent(void)
{
  uint32_t value = 0;
  
  for (int i = 0 ; i < NSAMPLES ; i++) {
    value += adcValueA1;
  }
  value /= NSAMPLES;
  Serial.print("adcValueA1: ");
  Serial.println(value);
  float current = (float)value * VREF / 4096 / SHUNT * 1000000;
  return current;
}

void readSPS(void)
{
  uint32_t value = 0;
  
  uint32_t start = millis();
  while (adcCounterA0 < TEST) {
    value += adcValueA0;
  }
  uint32_t elapsed = millis() - start;
  Serial.print("elapsed time ");
  Serial.print(elapsed);
  Serial.println(" µs");
  Serial.print("ADC speed: ");
  Serial.print(adcCounterA0 * 1000L / elapsed);
  Serial.println(" SPS");
}
