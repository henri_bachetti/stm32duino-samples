
#include "main.h"

ADC_HandleTypeDef    hadcVoltage;
ADC_HandleTypeDef    hadcCurrent;
__IO uint16_t adcValueA0, adcValueA1;
__IO int adcCounterA0, adcCounterA1;

ADC_ChannelConfTypeDef adcChannel[2] =
{
  {ADC_CHANNEL_3, 1, SAMPLE_TIME, 0},
  {ADC_CHANNEL_10, 1, SAMPLE_TIME, 0}
};

void adc_init_gpios(void)
{
  GPIO_InitTypeDef GPIO_InitStruct;

  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  GPIO_InitStruct.Pin = GPIO_PIN_3;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
  GPIO_InitStruct.Pin = GPIO_PIN_0;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
}

int adc_init(void)
{
  __HAL_RCC_ADC1_CLK_ENABLE();
  __HAL_RCC_ADC2_CLK_ENABLE();
  HAL_NVIC_SetPriority(ADC_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(ADC_IRQn);
  hadcVoltage.Instance                   = ADC1;
  hadcVoltage.Init.ClockPrescaler        = ADC_CLOCK_DIV;
  hadcVoltage.Init.Resolution            = ADC_RESOLUTION_12B;
  hadcVoltage.Init.ScanConvMode          = DISABLE;                       /* Sequencer disabled (ADC conversion on only 1 channel: channel set on rank 1) */
  hadcVoltage.Init.ContinuousConvMode    = ENABLE;                        /* Continuous mode disabled to have only 1 conversion at each conversion trig */
  hadcVoltage.Init.DiscontinuousConvMode = DISABLE;                       /* Parameter discarded because sequencer is disabled */
  hadcVoltage.Init.NbrOfDiscConversion   = 0;
  hadcVoltage.Init.ExternalTrigConvEdge  = ADC_EXTERNALTRIGCONVEDGE_NONE;        /* Conversion start trigged at each external event */
  hadcVoltage.Init.ExternalTrigConv      = ADC_EXTERNALTRIGCONV_T1_CC1;
  hadcVoltage.Init.DataAlign             = ADC_DATAALIGN_RIGHT;
  hadcVoltage.Init.NbrOfConversion       = 1;
  hadcVoltage.Init.DMAContinuousRequests = DISABLE;
  hadcVoltage.Init.EOCSelection          = DISABLE;
  if (HAL_ADC_Init(&hadcVoltage) != HAL_OK) {
    return false;
  }
  if (HAL_ADC_ConfigChannel(&hadcVoltage, &adcChannel[0]) != HAL_OK) {
    return false;
  }
  if (HAL_ADC_Start_IT(&hadcVoltage) != HAL_OK) {
    return false;
  }
  hadcCurrent.Instance                   = ADC2;
  hadcCurrent.Init.ClockPrescaler        = ADC_CLOCK_DIV;
  hadcCurrent.Init.Resolution            = ADC_RESOLUTION_12B;
  hadcCurrent.Init.ScanConvMode          = DISABLE;                       /* Sequencer disabled (ADC conversion on only 1 channel: channel set on rank 1) */
  hadcCurrent.Init.ContinuousConvMode    = ENABLE;                        /* Continuous mode disabled to have only 1 conversion at each conversion trig */
  hadcCurrent.Init.DiscontinuousConvMode = DISABLE;                       /* Parameter discarded because sequencer is disabled */
  hadcCurrent.Init.NbrOfDiscConversion   = 0;
  hadcCurrent.Init.ExternalTrigConvEdge  = ADC_EXTERNALTRIGCONVEDGE_NONE;        /* Conversion start trigged at each external event */
  hadcCurrent.Init.ExternalTrigConv      = ADC_EXTERNALTRIGCONV_T1_CC1;
  hadcCurrent.Init.DataAlign             = ADC_DATAALIGN_RIGHT;
  hadcCurrent.Init.NbrOfConversion       = 1;
  hadcCurrent.Init.DMAContinuousRequests = DISABLE;
  hadcCurrent.Init.EOCSelection          = DISABLE;
  if (HAL_ADC_Init(&hadcCurrent) != HAL_OK) {
    return false;
  }
  if (HAL_ADC_ConfigChannel(&hadcCurrent, &adcChannel[1]) != HAL_OK) {
    return 0;
  }
  if (HAL_ADC_Start_IT(&hadcCurrent) != HAL_OK) {
    return false;
  }
  return true;
}

extern "C" {

  void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* adcHandle)
  {
    if (adcHandle == &hadcVoltage) {
      adcValueA0 = HAL_ADC_GetValue(&hadcVoltage);
      adcCounterA0++;
    }
    else {
      adcValueA1 = HAL_ADC_GetValue(&hadcCurrent);
      adcCounterA1++;
    }
  }

  void ADC_IRQHandler(void)
  {
    uint32_t flg = __HAL_ADC_GET_FLAG(&hadcVoltage, ADC_FLAG_EOC);
    if (flg) {
      HAL_ADC_IRQHandler(&hadcVoltage);
    }
    flg = __HAL_ADC_GET_FLAG(&hadcCurrent, ADC_FLAG_EOC);
    if (flg) {
      HAL_ADC_IRQHandler(&hadcCurrent);
    }
  }
}