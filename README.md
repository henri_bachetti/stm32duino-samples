# STM32DUINO SAMPLES

The purpose of this page is to propose examples of code running on STM32 using STM32DUINO.

## ADC

3 examples :

### ADC read on A0 and A1 using polling on 1 ADC
   Board used: NUCLEO F401RE 

#### One channel

   ADC sample time 480 cycles:   85333 samples/s

   ADC sample time 122 cycles:   341333 samples/s

   ADC sample time 28 cycles:    744727 samples/s

#### Two channels

   ADC sample time 480 cycles:   20352 samples/s

   ADC sample time 122 cycles:   24899 samples/s

   ADC sample time 28 cycles:    25965 samples/s

### ADC read on A01 and A1 using polling on 2 ADCs

   Board used: NUCLEO L476RG

   ADC sample time 47.5 cycles:  133333 samples/s

   ADC sample time 12.5 cycles:  256410 samples/s

### ADC read on A0 and A1 using polling on 2 ADCs

   Board used: NUCLEO F429ZI

   ADC sample time 56 cycles:    312500 samples/s (per channel)

   ADC sample time 15 cycles:    833333 samples/s (per channel)

### ADC read on A0 and A1 using interrupts on 2 ADCs

   Board used: NUCLEO F429ZI

   ADC sample time 56 cycles:    313406 samples/s (per channel)

   ADC sample time 28 cycles:    543315 samples/s (per channel)

   ADC sample time 15 cycles:    main thread never executes

### ADC read on A0 and A1 using DMA on 1 ADC

   Board used: NUCLEO F429ZI

   ADC sample time 56 cycles:    156328 samples/s

   ADC sample time 28 cycles:    263473 samples/s

   ADC sample time 15 cycles:    401360 samples/s

### IDE

The code is build using ARDUINO IDE 1.8.5 or PlatformIO 1.17.1.

### BLOG

A description in french here :

https://riton-duino.blogspot.com/2018/03/stm32f103c8t6-et-arduino.html

https://riton-duino.blogspot.com/2019/03/stm32-environnements-de-developpement.html

https://riton-duino.blogspot.com/2019/03/stm32-boitier-st-link.html

https://riton-duino.blogspot.com/2019/03/stm32-duino-deboguer.html

